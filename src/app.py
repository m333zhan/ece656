import tkinter as tk
import pandas as pd
import matplotlib.pyplot as plt
import joblib
from sqlalchemy import create_engine
from sqlalchemy import text
from tkinter import ttk
from tkinter import scrolledtext
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from appConst import appConst as AppConst

# Create the database engine
g_engine = create_engine(AppConst.DBConst.db_connection_string)

# Create a figure and a subplot for the pie chart
g_fig_pie, g_ax_pie = plt.subplots(1, 3)

# Create a figure and a subplot for the bar chart
g_fig_bar, g_ax_bar = plt.subplots()

# Set the size of the figure
g_fig_pie.set_size_inches(8, 3)
g_fig_bar.set_size_inches(4, 4)

# Set the font size for all text elements
plt.rcParams.update({'font.size': 8})


def create_conn():
    # MySQL connection
    return g_engine.connect()


class App:
    # App
    def __init__(self, root):
        self.root = root

        # Create a custom font
        self.title_font = ("Helvetica", 20)
        self.button_text_font = ("Helvetica", 15)
        self.small_text = ("Helvetica", 8)
        self.big_text = ("Helvetica", 30)

        # Create a stack view and add it to the root window
        self.stack_view = AppConst.StackView(self.root, bg="grey")
        self.stack_view.pack(expand=True, fill="both")
        self.stack_view.grid_columnconfigure(0, weight=1)

        # Create frames
        self.start_frame = tk.Frame(self.stack_view, bg="grey")
        self.update_name_frame = tk.Frame(self.stack_view, bg="grey")
        self.champion_frame = tk.Frame(self.stack_view, bg="grey")
        self.item_frame = tk.Frame(self.stack_view, bg="grey")
        self.match_frame = tk.Frame(self.stack_view, bg="grey")
        self.match_pred_frame = tk.Frame(self.stack_view, bg="grey")

        # Init frames
        self.init_start_frame()
        self.init_champion_frame()
        self.init_item_frame()
        self.init_match_frame()
        self.init_match_pred_frame()
        self.init_update_name_frame()

        # Add the start_frame to the stack view
        self.stack_view.push(self.start_frame)

        # Load the saved model
        print("Loading the pre-trained model...")
        self.loaded_model = joblib.load('model/model.pkl')

        # Add the close function to the window
        self.root.protocol("WM_DELETE_WINDOW", self.close_window)

    def close_window(self):
        # Close the window clean up
        plt.close('all')
        self.root.destroy()

    def init_start_frame(self):
        # Create three frames to add to the stack view
        self.title_label = tk.Label(
            self.start_frame, text="League of Legend - Data analytics", font=self.title_font)
        self.update_name_button = tk.Button(self.start_frame, text="Update Name", font=self.button_text_font,
                                            command=lambda: self.stack_view.push(self.update_name_frame))
        self.champion_page_button = tk.Button(self.start_frame, text="Champion Info", font=self.button_text_font,
                                              command=lambda: self.stack_view.push(self.champion_frame))
        self.item_page_button = tk.Button(self.start_frame, text="Item Info", font=self.button_text_font,
                                          command=lambda: self.stack_view.push(self.item_frame))
        self.match_page_button = tk.Button(self.start_frame, text="Match Data Info", font=self.button_text_font,
                                           command=lambda: self.stack_view.push(self.match_frame))
        self.match_pred_page_button = tk.Button(self.start_frame, text="Who will be the winner?", font=self.button_text_font,
                                                command=lambda: self.stack_view.push(self.match_pred_frame))

        self.title_label.place(relx=0.5, rely=0.2, anchor="center")
        self.update_name_button.place(relx=0.5, rely=0.3, anchor="center")
        self.item_page_button.place(relx=0.5, rely=0.4, anchor="center")
        self.champion_page_button.place(relx=0.5, rely=0.5, anchor="center")
        self.match_page_button.place(relx=0.5, rely=0.6, anchor="center")
        self.match_pred_page_button.place(relx=0.5, rely=0.7, anchor="center")

    def init_champion_frame(self):
        # Config row
        self.champion_frame.rowconfigure(1, minsize=200)
        self.champion_frame.rowconfigure((0, 2, 3), minsize=50)
        self.champion_frame.grid_columnconfigure(0, weight=1)

        # Create a Treeview with three columns
        self.champion_table = ttk.Treeview(self.champion_frame, columns=(
            "ID", "Name", "Title", "Partype", "Difficulty", "Roles"), show="headings")
        self.back_button_champion = tk.Button(self.champion_frame, text="Back", font=self.button_text_font,
                                              command=lambda: self.stack_view.pop())
        self.details_champion_label = tk.Label(
            self.champion_frame, text="Check a champion's status", font=self.button_text_font)
        self.details_champion_text_input = AppConst.EntryWithPlaceholder(
            self.champion_frame, placeholder="Enter champion ID.")
        self.details_champion_check_button = tk.Button(self.champion_frame, text="Check", font=self.button_text_font,
                                                       command=lambda: self.show_detailes_champion())
        self.champion_story_label = scrolledtext.ScrolledText(
            self.champion_frame, width=100, height=10, wrap=tk.WORD, font=self.small_text)
        self.champion_details_table = ttk.Treeview(
            self.champion_frame, show="headings")

        # Set column headings
        self.champion_table.heading("ID", text="ID")
        self.champion_table.heading("Name", text="Name")
        self.champion_table.heading("Title", text="Title")
        self.champion_table.heading("Partype", text="Partype")
        self.champion_table.heading("Difficulty", text="Difficulty")
        self.champion_table.heading("Roles", text="Roles")

        # set the width of all columns
        self.champion_table.column("ID", width=50)
        self.champion_table.column("Name", width=160)
        self.champion_table.column("Title", width=160)
        self.champion_table.column("Partype", width=160)
        self.champion_table.column("Difficulty", width=160)
        self.champion_table.column("Roles", width=160)

        # Place the components into the window
        self.back_button_champion.grid(row=0, column=0)
        self.champion_table.grid(row=1, column=0)
        self.details_champion_label.grid(row=2, column=0)
        self.details_champion_text_input.grid(row=3, column=0)
        self.details_champion_check_button.grid(row=4, column=0)

        with create_conn() as cnx:
            df_champion = pd.read_sql(
                text(AppConst.AppQuery.champion_query), cnx)

            # Add data from the DataFrame to the table
            for i, row in df_champion.iterrows():
                self.champion_table.insert("", "end", values=(
                    int(row['ID']), row['Name'], row['Title'], row['Partype'], row['Difficulty'], row['Roles']))

    def show_detailes_champion(self):
        if self.check_champion_id_valid(self.details_champion_text_input.get()):
            # Clear all the rows in the Treeview
            self.champion_details_table.delete(
                *self.champion_details_table.get_children())
            self.champion_story_label.delete('1.0', tk.END)
            self.champion_story_label.grid(row=5, column=0)
            self.champion_details_table.grid(row=6, column=0)

            # Check champion details
            with create_conn() as cnx:
                df_champion_details = pd.read_sql(text(AppConst.AppQuery.champion_details_query), cnx, params={
                                                  "ID": self.details_champion_text_input.get()})

                df_champion_story = pd.read_sql(text(AppConst.AppQuery.champion_story_query), cnx, params={
                    "ID": self.details_champion_text_input.get()})

                # Insert champion story
                for i, row in df_champion_story.iterrows():
                    self.champion_story_label.insert(tk.INSERT, row['blurb'])

                # Create columns in the Treeview widget using the column names from the dataframe
                self.champion_details_table['columns'] = list(
                    df_champion_details.columns)
                for col in df_champion_details.columns:
                    self.champion_details_table.heading(col, text=col)

                # Add data from the DataFrame to the table
                for i, row in df_champion_details.iterrows():
                    self.champion_details_table.insert(
                        "", "end", values=list(row))
        else:
            self.details_champion_text_input.delete(0, tk.END)
            self.details_champion_text_input.insert(
                0, "Invalid Champion ID!!!!")

    def init_item_frame(self):
        # Config row
        self.item_frame.rowconfigure(1, minsize=200)
        self.item_frame.rowconfigure((0, 2, 3), minsize=50)
        self.item_frame.grid_columnconfigure(0, weight=1)

        # Components
        self.back_button_item = tk.Button(self.item_frame, text="Back", font=self.button_text_font,
                                          command=lambda: self.stack_view.pop())
        self.all_items_table = ttk.Treeview(
            self.item_frame, show="headings")
        self.find_item_through_name = tk.Label(
            self.item_frame, text="Search a item.", font=self.button_text_font)
        self.item_text_input = tk.Entry(
            self.item_frame, width=50)
        self.item_check_button = tk.Button(self.item_frame, text="Check", font=self.button_text_font,
                                           command=lambda: self.show_searched_item())
        self.searched_item_table = ttk.Treeview(
            self.item_frame, show="headings")

        # Place the components into the window
        self.back_button_item.grid(row=0, column=0)
        self.all_items_table.grid(row=1, column=0)
        self.find_item_through_name.grid(row=2, column=0)
        self.item_text_input.grid(row=3, column=0)
        self.item_check_button.grid(row=4, column=0)

        # Show all items
        with create_conn() as cnx:
            df_all_items = pd.read_sql(
                text(AppConst.AppQuery.item_all_query), cnx)

            # Create columns in the Treeview widget using the column names from the dataframe
            self.all_items_table['columns'] = list(
                df_all_items.columns)
            for col in df_all_items.columns:
                self.all_items_table.heading(col, text=col)

            # Add data from the DataFrame to the table
            for i, row in df_all_items.iterrows():
                row['itemID'] = int(row['itemID'])
                self.all_items_table.insert("", "end", values=list(row))

    def show_searched_item(self):
        # Clear all the rows in the searched_item_table Treeview
        self.searched_item_table.delete(
            *self.searched_item_table.get_children())
        self.searched_item_table.grid(row=5, column=0)

        # Show searched items details
        with create_conn() as cnx:
            df_searched_item = pd.read_sql(text(AppConst.AppQuery.item_search_query), cnx, params={
                "SEARCHNAME": '%' + self.item_text_input.get() + '%'})

            # Create columns in the Treeview widget using the column names from the dataframe
            self.searched_item_table['columns'] = list(
                df_searched_item.columns)
            for col in df_searched_item.columns:
                self.searched_item_table.heading(col, text=col)

            # Add data from the DataFrame to the table
            for i, row in df_searched_item.iterrows():
                row['itemID'] = int(row['itemID'])
                self.searched_item_table.insert("", "end", values=list(row))

    def init_user_match_status_frame(self):
        # Config
        self.user_match_status_frame.grid_columnconfigure(0, weight=1)

        # Components
        self.back_button_user_match_status = tk.Button(self.user_match_status_frame, text="Back", font=self.button_text_font,
                                                       command=lambda: self.stack_view.pop())
        self.name_label = tk.Label(
            self.user_match_status_frame, font=self.title_font)
        self.numbers_of_games_played_label = tk.Label(
            self.user_match_status_frame, font=self.small_text)
        self.death_kill_assist_table = ttk.Treeview(
            self.user_match_status_frame, show="headings")
        self.winning_rate_label = tk.Label(
            self.user_match_status_frame, font=self.small_text)
        self.different_kills_table = ttk.Treeview(
            self.user_match_status_frame, show="headings")
        self.pie_chart_canvas = FigureCanvasTkAgg(
            g_fig_pie, master=self.user_match_status_frame)
        self.user_match_status_history_table = ttk.Treeview(
            self.user_match_status_frame, show="headings")
        self.check_game_label = tk.Label(
            self.user_match_status_frame, font=self.button_text_font, text="Check game deatils using ID.")
        self.check_game_text_input = tk.Entry(
            self.user_match_status_frame, width=50)
        self.check_game_button = tk.Button(self.user_match_status_frame, text="Check", font=self.button_text_font,
                                           command=lambda: self.show_user_game_status_frame())

        # Place the components into the window
        self.back_button_user_match_status.grid(row=0, column=0)
        self.name_label.grid(row=1, column=0)
        self.numbers_of_games_played_label.grid(row=2, column=0)
        self.death_kill_assist_table.grid(row=3, column=0)
        self.winning_rate_label.grid(row=4, column=0)
        self.different_kills_table.grid(row=5, column=0)
        self.pie_chart_canvas.get_tk_widget().grid(row=6, column=0)
        self.user_match_status_history_table.grid(row=7, column=0)
        self.check_game_label.grid(row=8, column=0)
        self.check_game_text_input.grid(row=9, column=0)
        self.check_game_button.grid(row=10, column=0)

        self.death_kill_assist_table.grid_propagate(False)
        self.death_kill_assist_table.config(height=1)
        self.different_kills_table.grid_propagate(False)
        self.different_kills_table.config(height=1)

    def init_user_game_status_frame(self):
        # Config
        self.user_game_status_frame.grid_columnconfigure(0, weight=1)

        # Components
        self.back_button_user_game_status = tk.Button(self.user_game_status_frame, text="Back", font=self.button_text_font,
                                                      command=lambda: self.stack_view.pop())
        self.game_status_label = tk.Label(
            self.user_game_status_frame, font=self.button_text_font, text='Your game status')
        self.user_game_status_table = ttk.Treeview(
            self.user_game_status_frame, columns=('data',), show='tree')
        self.game_teams_status_label = tk.Label(
            self.user_game_status_frame, font=self.button_text_font, text='Your team/enemy status')
        self.user_team_status_table = ttk.Treeview(
            self.user_game_status_frame, columns=('data',), show='tree')
        self.user_enemy_team_status_table = ttk.Treeview(
            self.user_game_status_frame, columns=('data',), show='tree')
        self.gold_compare_label = tk.Label(
            self.user_game_status_frame, font=self.button_text_font, text='Your team/enemy gold status.')
        self.bar_chart_canvas = FigureCanvasTkAgg(
            g_fig_bar, master=self.user_game_status_frame)

        # Place the components into the window
        self.back_button_user_game_status.grid(row=0, column=0, columnspan=2)
        self.game_status_label.grid(row=1, column=0, columnspan=2)
        self.user_game_status_table.grid(row=2, column=0, columnspan=2)
        self.game_teams_status_label.grid(row=3, column=0, columnspan=2)
        self.user_team_status_table.grid(row=4, column=0)
        self.user_enemy_team_status_table.grid(row=4, column=1)
        self.bar_chart_canvas.get_tk_widget().grid(row=5, column=0, columnspan=2)

        self.user_game_status_table.grid_propagate(False)
        self.user_game_status_table.config(height=15)
        self.user_team_status_table.grid_propagate(False)
        self.user_team_status_table.config(height=5)
        self.user_enemy_team_status_table.grid_propagate(False)
        self.user_enemy_team_status_table.config(height=5)

    def init_match_frame(self):
        # Match frame should have three sub frame place on stack
        self.user_match_status_frame = tk.Frame(self.stack_view, bg="grey")
        self.user_game_status_frame = tk.Frame(self.stack_view, bg="grey")

        # Init sub-frames
        self.init_user_match_status_frame()
        self.init_user_game_status_frame()

        # Components
        self.back_button_match_root = tk.Button(self.match_frame, text="Back", font=self.button_text_font,
                                                command=lambda: self.stack_view.pop())
        self.match_lol_id_search_label = tk.Label(
            self.match_frame, text="Check your match status by entering LOL ID.", font=self.button_text_font)
        self.match_lol_id_search_text_input = tk.Entry(
            self.match_frame, width=50)
        self.match_lol_id_search_check_button = tk.Button(self.match_frame, text="Check", font=self.button_text_font,
                                                          command=lambda: self.show_user_match_status_frame())

        self.back_button_match_root.place(relx=0.5, rely=0.1, anchor="center")
        self.match_lol_id_search_label.place(
            relx=0.5, rely=0.4, anchor="center")
        self.match_lol_id_search_text_input.place(
            relx=0.5, rely=0.5, anchor="center")
        self.match_lol_id_search_check_button.place(
            relx=0.5, rely=0.6, anchor="center")

    def show_user_match_status_frame(self):
        if self.check_lol_id_valid(self.match_lol_id_search_text_input.get()):
            # Push this frame
            self.stack_view.push(self.user_match_status_frame)

            # Clear all the rows in the searched_item_table Treeview
            self.death_kill_assist_table.delete(
                *self.death_kill_assist_table.get_children())
            self.different_kills_table.delete(
                *self.different_kills_table.get_children())
            self.user_match_status_history_table.delete(
                *self.user_match_status_history_table.get_children())

            # Show user match status
            with create_conn() as cnx:
                df_user_name = pd.read_sql(text(AppConst.AppQuery.user_name_query), cnx, params={
                    "LOLID": self.match_lol_id_search_text_input.get()})
                df_user_winning_rate = pd.read_sql(text(AppConst.AppQuery.user_win_rate_query), cnx, params={
                    "LOLID": self.match_lol_id_search_text_input.get()})
                df_user_history_match = pd.read_sql(text(AppConst.AppQuery.user_history_match_query), cnx, params={
                    "LOLID": self.match_lol_id_search_text_input.get()})
                df_user_special_kills = pd.read_sql(text(AppConst.AppQuery.user_special_kills_query), cnx, params={
                    "LOLID": self.match_lol_id_search_text_input.get()})
                df_user_champion_usage = pd.read_sql(text(AppConst.AppQuery.user_champion_usage_query), cnx, params={
                    "LOLID": self.match_lol_id_search_text_input.get()})

            # Insert usr name
            for i, row in df_user_name.iterrows():
                self.name_label.config(text=row['summonerName'])
            # Number of games player
            self.numbers_of_games_played_label.config(
                text='Number of games played: ' + str(df_user_history_match.size))
            # Winning rate
            self.winning_rate_label.config(
                text='Winning Rate: ' + "{0:.0%}".format(df_user_winning_rate['win_rate'].values[0]))

            # Create columns in the Treeview widget using the column names from the dataframe
            self.death_kill_assist_table['columns'] = [
                'totalKills', 'totalDeaths', 'totalAssists']
            self.different_kills_table['columns'] = [
                'totalDoubleKills', 'totalTripleKills', 'totalQuadraKills', 'totalPentaKills']
            self.user_match_status_history_table['columns'] = list(
                df_user_history_match.columns)
            for col in df_user_special_kills.columns:
                if col in ['totalKills', 'totalDeaths', 'totalAssists']:
                    self.death_kill_assist_table.heading(col, text=col)
                else:
                    self.different_kills_table.heading(col, text=col)

            for col in df_user_history_match.columns:
                self.user_match_status_history_table.heading(col, text=col)

            # Add data from the DataFrame to the table
            for i, row in df_user_special_kills.iterrows():
                self.death_kill_assist_table.insert(
                    "", "end", values=(int(row['totalKills']), int(row['totalDeaths']), int(row['totalAssists'])))
                self.different_kills_table.insert(
                    "", "end", values=(int(row['totalDoubleKills']), int(row['totalTripleKills']),
                                       int(row['totalQuadraKills']), int(row['totalPentaKills'])))

            for i, row in df_user_history_match.iterrows():
                row['gameID'] = int(row['gameID'])
                row['win'] = True if int.from_bytes(
                    row['win'], 'little') == 1 else False
                self.user_match_status_history_table.insert(
                    "", "end", values=list(row))

            # Clear graph
            g_ax_pie[0].cla()
            g_ax_pie[1].cla()
            g_ax_pie[2].cla()

            # Plot the data as a pie chart
            g_ax_pie[0].pie(df_user_champion_usage['ChampionUsage'].tolist(),
                            labels=df_user_champion_usage['ChampionName'].tolist())
            g_ax_pie[0].set_title('Champion usage chart - Top ten')

            top = df_user_history_match[df_user_history_match['lane']
                                        == 'TOP'].shape[0]
            mid = df_user_history_match[df_user_history_match['lane']
                                        == 'MIDDLE'].shape[0]
            bottom = df_user_history_match[df_user_history_match['lane']
                                           == 'BOTTOM'].shape[0]
            jungle = df_user_history_match[df_user_history_match['lane']
                                           == 'JUNGLE'].shape[0]
            support = df_user_history_match[df_user_history_match['lane']
                                            == 'NONE'].shape[0]

            DUO = df_user_history_match[df_user_history_match['role']
                                        == 'DUO'].shape[0]
            SOLO = df_user_history_match[df_user_history_match['role']
                                         == 'SOLO'].shape[0]
            DUO_CARRY = df_user_history_match[df_user_history_match['role']
                                              == 'DUO_CARRY'].shape[0]
            DUO_SUPPORT = df_user_history_match[df_user_history_match['role']
                                                == 'DUO_SUPPORT'].shape[0]
            JUNGLE = df_user_history_match[df_user_history_match['role']
                                           == 'NONE'].shape[0]

            g_ax_pie[1].pie([top, mid, bottom, jungle, support],
                            labels=['Top', 'Mid', 'Bottom', 'Jungle', 'None (support)'])
            g_ax_pie[1].set_title('Lane chart')
            g_ax_pie[2].pie([DUO, SOLO, DUO_CARRY, DUO_SUPPORT, JUNGLE],
                            labels=['Duo', 'Solo', 'Duo_Carry', 'Duo_Support', 'Jungle'])
            g_ax_pie[2].set_title('Role chart')
            self.pie_chart_canvas.draw()
        else:
            self.match_lol_id_search_text_input.delete(0, tk.END)
            self.match_lol_id_search_text_input.insert(0, "Invalid ID!!!!")

    def show_user_game_status_frame(self):
        if self.check_game_id_valid(self.check_game_text_input.get(), self.match_lol_id_search_text_input.get()):
            # Push this frame
            self.stack_view.push(self.user_game_status_frame)

            # Clear all the rows in the Treeview
            self.user_game_status_table.delete(
                *self.user_game_status_table.get_children())
            self.user_team_status_table.delete(
                *self.user_team_status_table.get_children())
            self.user_enemy_team_status_table.delete(
                *self.user_enemy_team_status_table.get_children())

            # Check game details
            with create_conn() as cnx:
                df_game_details = pd.read_sql(text(AppConst.AppQuery.user_game_status_query), cnx, params={
                    "LOLID": self.match_lol_id_search_text_input.get(),
                    "GAMEID": self.check_game_text_input.get()})
                df_game_team_details = pd.read_sql(text(AppConst.AppQuery.user_team_status_query), cnx, params={
                    "LOLID": self.match_lol_id_search_text_input.get(),
                    "GAMEID": self.check_game_text_input.get()})
                df_game_enemy_team_details = pd.read_sql(text(AppConst.AppQuery.user_enemy_team_status_query), cnx, params={
                    "LOLID": self.match_lol_id_search_text_input.get(),
                    "GAMEID": self.check_game_text_input.get()})
                df_user_team_gold = pd.read_sql(text(AppConst.AppQuery.user_team_gold_query), cnx, params={
                    "LOLID": self.match_lol_id_search_text_input.get(),
                    "GAMEID": self.check_game_text_input.get(),
                    "TEAMID": df_game_team_details['teamID'].values[0]})
                df_user_enemy_team_gold = pd.read_sql(text(AppConst.AppQuery.user_team_gold_query), cnx, params={
                    "LOLID": self.match_lol_id_search_text_input.get(),
                    "GAMEID": self.check_game_text_input.get(),
                    "TEAMID": df_game_enemy_team_details['teamID'].values[0]})

            # Add data from the DataFrame to the table
            for i, row in df_game_details.iterrows():
                for col in df_game_details.columns:
                    if type(row[col]) is bytes:
                        row[col] = True if int.from_bytes(
                            row[col], 'little') == 1 else False
                    else:
                        row[col] = int(row[col])
                    self.user_game_status_table.insert(
                        "", "end", text=col, values=row[col])

            # Add data from the DataFrame to the table
            for i, row in df_game_team_details.iterrows():
                for col in df_game_team_details.columns:
                    if col == 'teamID':
                        continue

                    if type(row[col]) is bytes:
                        row[col] = True if int.from_bytes(
                            row[col], 'little') == 1 else False
                    else:
                        row[col] = int(row[col])
                    self.user_team_status_table.insert(
                        "", "end", text=col, values=row[col])

            # Add data from the DataFrame to the table
            for i, row in df_game_enemy_team_details.iterrows():
                for col in df_game_enemy_team_details.columns:
                    if col == 'teamID':
                        continue

                    if type(row[col]) is bytes:
                        row[col] = True if int.from_bytes(
                            row[col], 'little') == 1 else False
                    else:
                        row[col] = int(row[col])
                    self.user_enemy_team_status_table.insert(
                        "", "end", text=col, values=row[col])

            # Clear graph
            g_ax_bar.cla()
            # Position of each bar on the x-axis
            r1 = [0]
            r2 = [AppConst.MiscConst.bar_width]
            r3 = [1]
            r4 = [1 + AppConst.MiscConst.bar_width]

            # Plot goldSpent and goldEarned
            g_ax_bar.bar(r1, df_user_team_gold['totalGoldEarned'].values, color='r',
                         width=AppConst.MiscConst.bar_width, label='Team Gold Earned')
            g_ax_bar.bar(r2, df_user_enemy_team_gold['totalGoldEarned'].values, color='g',
                         width=AppConst.MiscConst.bar_width, label='Enemy Gold Earned')
            g_ax_bar.bar(r3, df_user_team_gold['totalGoldSpent'].values, color='b',
                         width=AppConst.MiscConst.bar_width, label='Team Gold Spent')
            g_ax_bar.bar(r4, df_user_enemy_team_gold['totalGoldSpent'].values, color='y',
                         width=AppConst.MiscConst.bar_width, label='Enemy Gold Spent')

            # Add title and legend
            g_ax_bar.set_title('Gold Spent and Gold Earned')
            g_ax_bar.legend()
            self.bar_chart_canvas.draw()
        else:
            self.check_game_text_input.delete(0, tk.END)
            self.check_game_text_input.insert(0, "Invalid ID!!!!")

    def init_match_pred_frame(self):
        # Define a function to check the entry widget values
        def check_entries():
            if not self.tower_kills_text_input.check_empty() or \
               not self.inhibitor_kills_text_input.check_empty() or \
               not self.baron_kills_text_input.check_empty() or \
               not self.dragon_kills_text_input.check_empty() or \
               not self.vilemaw_kills_text_input.check_empty() or \
               not self.rift_herald_kills_text_input.check_empty() or \
               not self.dv_kills_text_input.check_empty():
                self.pred_button.config(state="disabled")
            else:
                self.pred_button.config(state="normal")

        # Custom validation function to allow only numeric input
        def validate_numeric_input(new_value):
            if new_value.isnumeric() or new_value == "":
                return True
            else:
                return False

        # Config row
        self.match_pred_frame.grid_rowconfigure(
            (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16), minsize=40)
        self.match_pred_frame.grid_columnconfigure(0, weight=1)

        # Create a IntVar to hold the selected state of the check button
        self.first_blood_var = tk.IntVar()
        self.first_tower_var = tk.IntVar()
        self.first_inhibitor_var = tk.IntVar()
        self.first_baron_var = tk.IntVar()
        self.first_dragon_var = tk.IntVar()
        self.first_rift_herald_var = tk.IntVar()

        # Components
        self.back_button_pred = tk.Button(self.match_pred_frame, text="Back", font=self.button_text_font,
                                          command=lambda: self.stack_view.pop())
        self.pred_title = tk.Label(
            self.match_pred_frame, text="Input your team's status, predict your victory!", font=self.title_font)
        self.first_blood_radio_button = tk.Checkbutton(
            self.match_pred_frame, text="First Blood", variable=self.first_blood_var)
        self.first_tower_radio_button = tk.Checkbutton(
            self.match_pred_frame, text="First Tower", variable=self.first_tower_var)
        self.first_inhibitor_radio_button = tk.Checkbutton(
            self.match_pred_frame, text="First Inhibitor", variable=self.first_inhibitor_var)
        self.first_baron_radio_button = tk.Checkbutton(
            self.match_pred_frame, text="First Baron", variable=self.first_baron_var)
        self.first_dragon_radio_button = tk.Checkbutton(
            self.match_pred_frame, text="First Dragon", variable=self.first_dragon_var)
        self.first_rift_herald_radio_button = tk.Checkbutton(
            self.match_pred_frame, text="First RiftHerald", variable=self.first_rift_herald_var)
        self.tower_kills_text_input = AppConst.EntryWithPlaceholder(
            self.match_pred_frame, placeholder='Enter the number of tower kills.')
        self.tower_kills_text_input.configure(width=30)
        self.inhibitor_kills_text_input = AppConst.EntryWithPlaceholder(
            self.match_pred_frame, placeholder='Enter the number of inhibitor kills.')
        self.inhibitor_kills_text_input.configure(width=30)
        self.baron_kills_text_input = AppConst.EntryWithPlaceholder(
            self.match_pred_frame, placeholder='Enter the number of baron kills.')
        self.baron_kills_text_input.configure(width=30)
        self.dragon_kills_text_input = AppConst.EntryWithPlaceholder(
            self.match_pred_frame, placeholder='Enter the number of dragon kills.')
        self.dragon_kills_text_input.configure(width=30)
        self.vilemaw_kills_text_input = AppConst.EntryWithPlaceholder(
            self.match_pred_frame, placeholder='Enter the number of vilemaw kills.')
        self.vilemaw_kills_text_input.configure(width=30)
        self.rift_herald_kills_text_input = AppConst.EntryWithPlaceholder(
            self.match_pred_frame, placeholder='Enter the number of rift herald kills.')
        self.rift_herald_kills_text_input.configure(width=30)
        self.dv_kills_text_input = AppConst.EntryWithPlaceholder(
            self.match_pred_frame, placeholder='Enter the number of dominion victories.')
        self.dv_kills_text_input.configure(width=35)
        self.pred_button = tk.Button(self.match_pred_frame, text="Predict!", font=self.button_text_font,
                                     command=lambda: self.start_prediction())
        self.result_label = tk.Label(
            self.match_pred_frame, font=self.big_text)

        # Set allow numeric value only
        self.tower_kills_text_input.config(validate="key", validatecommand=(
            self.match_pred_frame.register(validate_numeric_input), '%P'))
        self.inhibitor_kills_text_input.config(validate="key", validatecommand=(
            self.match_pred_frame.register(validate_numeric_input), '%P'))
        self.baron_kills_text_input.config(validate="key", validatecommand=(
            self.match_pred_frame.register(validate_numeric_input), '%P'))
        self.dragon_kills_text_input.config(validate="key", validatecommand=(
            self.match_pred_frame.register(validate_numeric_input), '%P'))
        self.vilemaw_kills_text_input.config(validate="key", validatecommand=(
            self.match_pred_frame.register(validate_numeric_input), '%P'))
        self.rift_herald_kills_text_input.config(validate="key", validatecommand=(
            self.match_pred_frame.register(validate_numeric_input), '%P'))
        self.dv_kills_text_input.config(validate="key", validatecommand=(
            self.match_pred_frame.register(validate_numeric_input), '%P'))

        # Place the components into the window
        self.back_button_pred.grid(row=0, column=0)
        self.pred_title.grid(row=1, column=0)
        self.first_blood_radio_button.grid(row=2, column=0)
        self.first_tower_radio_button.grid(row=3, column=0)
        self.first_inhibitor_radio_button.grid(row=4, column=0)
        self.first_baron_radio_button.grid(row=5, column=0)
        self.first_dragon_radio_button.grid(row=6, column=0)
        self.first_rift_herald_radio_button.grid(row=7, column=0)
        self.tower_kills_text_input.grid(row=8, column=0)
        self.inhibitor_kills_text_input.grid(row=9, column=0)
        self.baron_kills_text_input.grid(row=10, column=0)
        self.dragon_kills_text_input.grid(row=11, column=0)
        self.vilemaw_kills_text_input.grid(row=12, column=0)
        self.rift_herald_kills_text_input.grid(row=13, column=0)
        self.dv_kills_text_input.grid(row=14, column=0)
        self.pred_button.grid(row=15, column=0)

        # Call the check_entries() function whenever the contents of the entry widgets change
        self.tower_kills_text_input.bind(
            "<KeyRelease>", lambda event: check_entries())
        self.inhibitor_kills_text_input.bind(
            "<KeyRelease>", lambda event: check_entries())
        self.baron_kills_text_input.bind(
            "<KeyRelease>", lambda event: check_entries())
        self.dragon_kills_text_input.bind(
            "<KeyRelease>", lambda event: check_entries())
        self.vilemaw_kills_text_input.bind(
            "<KeyRelease>", lambda event: check_entries())
        self.rift_herald_kills_text_input.bind(
            "<KeyRelease>", lambda event: check_entries())
        self.dv_kills_text_input.bind(
            "<KeyRelease>", lambda event: check_entries())

        self.pred_button.config(state="disabled")

    def start_prediction(self):
        # Add Label to the grid
        self.result_label.grid(row=16, column=0)

        X_test = [[self.first_blood_var.get(),
                   self.first_tower_var.get(),
                   self.first_inhibitor_var.get(),
                   self.first_baron_var.get(),
                   self.first_dragon_var.get(),
                   self.first_rift_herald_var.get(),
                   int(self.tower_kills_text_input.get()),
                   int(self.inhibitor_kills_text_input.get()),
                   int(self.baron_kills_text_input.get()),
                   int(self.dragon_kills_text_input.get()),
                   int(self.vilemaw_kills_text_input.get()),
                   int(self.rift_herald_kills_text_input.get()),
                   int(self.dv_kills_text_input.get())]]

        df = pd.DataFrame(X_test, columns=AppConst.MiscConst.features_team)

        # Show result
        self.result_label.config(text='Victory!!!' if self.loaded_model.predict(df)[
                                 0] == 1 else "Lost...")

    def init_update_name_frame(self):
        # Config row
        self.update_name_frame.grid_columnconfigure(0, weight=1)

        # Components
        self.back_button_update_name = tk.Button(self.update_name_frame, text="Back", font=self.button_text_font,
                                                 command=lambda: self.stack_view.pop())
        self.update_name_lol_id_label = tk.Label(
            self.update_name_frame, text="Update Summoner Name by entering LOL ID.", font=self.button_text_font)
        self.update_name_lol_id_text_input = tk.Entry(
            self.update_name_frame, width=50)
        self.current_name_check_button = tk.Button(self.update_name_frame, text="Check", font=self.button_text_font,
                                                   command=lambda: self.check_name_call_back())
        self.current_name_label = tk.Label(
            self.update_name_frame, font=self.button_text_font)
        self.update_name_new_name_text_input = AppConst.EntryWithPlaceholder(
            self.update_name_frame, placeholder='New Name.')
        self.update_new_name_button = tk.Button(self.update_name_frame, text="Update", font=self.button_text_font,
                                                command=lambda: self.update_name_call_back())
        self.update_status_label = tk.Label(
            self.update_name_frame, font=self.button_text_font)

        # Initially, disabled
        self.update_new_name_button.config(state="disabled")

        # Call the check_entries() function whenever the contents of the entry widgets change
        self.update_name_new_name_text_input.bind(
            "<KeyRelease>", lambda event: check_entries())

        # Place the components into the window
        self.back_button_update_name.place(relx=0.5, rely=0.1, anchor="center")
        self.update_name_lol_id_label.place(
            relx=0.5, rely=0.2, anchor="center")
        self.update_name_lol_id_text_input.place(
            relx=0.5, rely=0.3, anchor="center")
        self.current_name_check_button.place(
            relx=0.5, rely=0.4, anchor="center")

        # Define a function to check the entry widget values
        def check_entries():
            if not self.update_name_new_name_text_input.check_empty():
                self.update_new_name_button.config(state="disabled")
            else:
                self.update_new_name_button.config(state="normal")

    def check_name_call_back(self):
        if self.check_lol_id_valid(self.update_name_lol_id_text_input.get()):
            # Check current user summoner name
            with create_conn() as cnx:
                df_summoner_name = pd.read_sql(text(AppConst.AppQuery.check_name_query), cnx, params={
                    "LOLID": self.update_name_lol_id_text_input.get()})

            self.current_name_label.config(
                text=df_summoner_name['summonerName'].values[0])
            self.current_name_label.place(
                relx=0.5, rely=0.5, anchor="center")
            self.update_name_new_name_text_input.place(
                relx=0.5, rely=0.6, anchor="center")
            self.update_new_name_button.place(
                relx=0.5, rely=0.7, anchor="center")
        else:
            self.update_name_lol_id_text_input.delete(0, tk.END)
            self.update_name_lol_id_text_input.insert(0, "Invalid ID!!!!")

    def update_name_call_back(self):
        # Update summoner name through LOLID
        with create_conn() as cnx:
            cnx.execute(text(AppConst.AppQuery.update_name_query), parameters={
                "LOLID": self.update_name_lol_id_text_input.get(),
                "NEWNAME": self.update_name_new_name_text_input.get()})
            df_new_summoner_name = pd.read_sql(text(AppConst.AppQuery.check_name_query), cnx, params={
                "LOLID": self.update_name_lol_id_text_input.get()})

            # Commit change
            cnx.commit()

        if df_new_summoner_name['summonerName'].values[0] == self.update_name_new_name_text_input.get():
            self.update_status_label.config(text="Done!")
            self.update_new_name_button.config(state="disabled")
            self.update_name_new_name_text_input.delete(0, tk.END)
        else:
            self.update_status_label.config(text="Failed!")

        self.update_status_label.place(
            relx=0.5, rely=0.8, anchor="center")

    def check_lol_id_valid(self, id):
        # Check whether the lol id exists
        with create_conn() as cnx:
            df_id_count = pd.read_sql(text(AppConst.AppQuery.check_lol_id_query), cnx, params={
                "LOLID": id})

        return False if df_id_count['COUNT(lolID)'].values[0] == 0 else True

    def check_champion_id_valid(self, id):
        # Check whether the champion id exists
        with create_conn() as cnx:
            df_id_count = pd.read_sql(text(AppConst.AppQuery.check_champion_id_query), cnx, params={
                "CHAMPIONID": id})

        return False if df_id_count['COUNT(championID)'].values[0] == 0 else True

    def check_game_id_valid(self, game_id, lol_id):
        # Check whether the champion id exists
        with create_conn() as cnx:
            df_id_count = pd.read_sql(text(AppConst.AppQuery.check_game_id_query), cnx, params={
                "GAMEID": game_id,
                "LOLID": lol_id})

        return False if df_id_count['COUNT(TeamParticipants.gameID)'].values[0] == 0 else True


# Run the application using the mainloop() method
if __name__ == '__main__':
    # Create a root window
    root = tk.Tk()
    root.title("League of Legends - Who will be the winner?")
    root.geometry("900x900")

    # Calculate the position of the root window to center it on the screen
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    x = (screen_width // 2) - (900 // 2)
    y = (screen_height // 2) - (900 // 2)

    # Set the position of the root window to center it on the screen
    root.geometry("+{}+{}".format(x, y))

    app = App(root)
    root.mainloop()
