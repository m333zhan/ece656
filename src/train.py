import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import joblib
import mysql.connector
from graphviz import render
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from appConst import appConst as AppConst

# Define the database configuration
db_config = {
    'host': AppConst.DBConst.db_connect_host,
    'user': AppConst.DBConst.db_connect_user_name,
    'port': AppConst.DBConst.db_connect_port,
    'password': AppConst.DBConst.db_connect_password,
    'database': AppConst.DBConst.db_connect_db_name
}

# Define Team data frame
df_team = None

# Select all Team table's data
try:
    # Connect to the database
    print("Loading Team table's data...")
    with mysql.connector.connect(**db_config) as connection:
        with connection.cursor() as cursor:
            query_select_data = "SELECT win, firstBlood, firstTower, firstInhibitor, "\
                "firstBaron, firstDragon, firstRiftHerald, towerKills, "\
                "inhibitorKills, baronKills, dragonKills, vilemawKills, "\
                "riftHeraldKills, dominionVictoryScore FROM Team"
            cursor.execute(query_select_data)
            rows = cursor.fetchall()
            df_team = pd.DataFrame.from_records(
                rows, columns=[desc[0] for desc in cursor.description])

except Exception as e:
    print("An error occurred when loading Team table's data:", e)

# Init label and features
label_team = ['win']
features_team = AppConst.MiscConst.features_team

# We will use the maximum depth of trees as the hyperparameter
max_depth_range = 40
depth_step = 1

# Split data into train and test in 80/20
X_train_team, X_test_team, y_train_team, y_test_team =\
    train_test_split(df_team.loc[:, df_team.columns != 'win'],
                     df_team['win'].values.ravel(), test_size=0.2)

# Use default Gini impurity
# By using K-fold cross validation to get accuracy scores and compare the scores,
# we find the best hyperparameter to train the decision tree
dt_team = DecisionTreeClassifier(max_depth=1)
dt_team_depth = 1
dt_team_score = 0.0

dt_depth_array_team = []
dt_score_array_team = []

print("Finding the best hyperparameter...")
for depth_val in np.arange(1, max_depth_range + 1, depth_step):
    dt_team = DecisionTreeClassifier(max_depth=depth_val)
    cv_scores = cross_val_score(
        dt_team, X_train_team, y_train_team.ravel(), cv=5)
    score = np.mean(cv_scores)

    dt_depth_array_team.append(depth_val)
    dt_score_array_team.append(score)

    if score > dt_team_score:
        dt_team.fit(X_train_team, y_train_team.ravel())
        dt_team_score = dt_team.score(
            X_test_team, y_test_team)
        dt_team_depth = depth_val

# Plot mean accuracy vs. relative to tree depth
plt.figure(dpi=100)
plt.plot(dt_depth_array_team, dt_score_array_team)
plt.title('Mean Accuracy vs. Tree Depth')
plt.xlabel('Tree Depth')
plt.ylabel('Mean Accuracy')
plt.show()

# Now we have the best depth, output the model, and its decision graph
dt_team = DecisionTreeClassifier(max_depth=dt_team_depth)
dt_team.fit(X_train_team, y_train_team.ravel())

print("Exporting the model...")
joblib.dump(dt_team, 'model/model.pkl')

# PNG file
dot_data = tree.export_graphviz(dt_team,
                                feature_names=X_train_team.columns,
                                filled=True,
                                impurity=False,
                                proportion=False,
                                class_names=list(
                                    map(str, df_team['win'].unique())),
                                out_file=None)

print("Exporting the dot file...")
# Save the Graphviz data to a file named "tree.dot"
with open("../doc/tree.dot", "w") as f:
    f.write(dot_data)

print("Exporting the decision tree PNG...")
# Render the tree.dot file to a PNG image named "tree.png"
render("dot", "png", "../doc/tree.dot", quiet=False)
