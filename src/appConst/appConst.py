import tkinter as tk


class DBConst(object):
    db_connect_host = 'localhost'
    db_connect_user_name = 'root'
    db_connect_password = 'secret'
    db_connect_port = 30306
    db_connect_db_name = 'lolstatus'
    db_connection_string = f'mysql+pymysql://{db_connect_user_name}:{db_connect_password}'\
                           f'@{db_connect_host}:{db_connect_port}/{db_connect_db_name}'


class MissingValueConst(object):
    bool_value = False
    int_value = 0


class MiscConst(object):
    max_match_data_part = 7
    bar_width = 0.2
    features_team = ['firstBlood', 'firstTower', 'firstInhibitor',
                     'firstBaron', 'firstDragon', 'firstRiftHerald', 'towerKills',
                     'inhibitorKills', 'baronKills', 'dragonKills', 'vilemawKills',
                     'riftHeraldKills', 'dominionVictoryScore']


class AppQuery(object):
    champion_query = 'SELECT Champion.championID as ID, Champion.name as Name, Champion.title as Title, '\
                     'Champion.partype as Partype, Champion.difficulty as Difficulty, Champion.tag as Roles '\
                     'FROM Champion'
    champion_story_query = 'SELECT blurb FROM Champion where championID = :ID'
    champion_details_query = 'SELECT baseAttack, baseDefense, baseMagic, hp, hpPerLevel, mp, '\
                             'mpPerLevel, moveSpeed, armor, armorPerLevel, spellBlock, spellBlockPerLevel, '\
                             'attackRange, hpRegen, hpRegenPerLevel, mpRegen, mpRegenPerLevel, attackDamage, '\
                             'attackDamagerPerLevel, attackSpeedPerLevel, attackSpeed FROM ChampionInfo where championID = :ID'

    item_all_query = 'SELECT * FROM Item'
    item_search_query = 'SELECT * FROM Item WHERE name LIKE :SEARCHNAME'

    check_lol_id_query = 'SELECT COUNT(lolID) FROM PlayerInfo WHERE lolID = :LOLID'
    check_champion_id_query = 'SELECT COUNT(championID) FROM Champion WHERE championID = :CHAMPIONID'
    check_game_id_query = 'SELECT COUNT(TeamParticipants.gameID) FROM TeamParticipants JOIN PlayerInfo ON '\
                          'PlayerInfo.summonerID = TeamParticipants.summonerID WHERE TeamParticipants.gameID = :GAMEID AND lolID = :LOLID'
    check_name_query = 'SELECT summonerName FROM PlayerInfo WHERE lolID = :LOLID'
    update_name_query = 'UPDATE PlayerInfo SET summonerName = :NEWNAME WHERE lolID = :LOLID'

    user_name_query = 'SELECT summonerName FROM PlayerInfo WHERE lolID = :LOLID'
    user_history_match_query = 'SELECT g.gameID as gameID, t.win as win, cp.name as championName, tp.role as role, tp.lane as lane '\
                               'FROM Game g JOIN Team t ON g.gameID = t.gameID JOIN TeamParticipants tp '\
                               'ON t.gameID = tp.gameID AND t.teamID = tp.teamID JOIN PlayerInfo pi '\
                               'ON tp.summonerID = pi.summonerID JOIN Champion cp ON cp.championID = tp.championID '\
                               'WHERE pi.lolID = :LOLID'
    user_special_kills_query = 'SELECT SUM(TPI.kills) AS totalKills, SUM(TPI.deaths) AS totalDeaths, SUM(TPI.assists) AS totalAssists, '\
                               'SUM(TPI.doubleKills) AS totalDoubleKills, SUM(TPI.tripleKills) AS totalTripleKills, '\
                               'SUM(TPI.quadraKills) AS totalQuadraKills, SUM(TPI.pentaKills) AS totalPentaKills '\
                               'FROM TeamParticipantInfo TPI JOIN PlayerInfo ON PlayerInfo.summonerID = TPI.summonerID '\
                               'JOIN TeamParticipants TP ON TPI.gameID = TP.gameID AND TP.summonerID = TPI.summonerID '\
                               'JOIN Champion ON TP.championID = Champion.championID WHERE lolID = :LOLID'
    user_win_rate_query = 'SELECT COUNT(CASE WHEN t.win = 1 THEN 1 END) / COUNT(*) AS win_rate '\
                          'FROM Game g JOIN Team t ON g.gameID = t.gameID JOIN TeamParticipants tp '\
                          'ON t.gameID = tp.gameID AND t.teamID = tp.teamID JOIN PlayerInfo pi '\
                          'ON tp.summonerID = pi.summonerID WHERE pi.lolID = :LOLID'
    user_champion_usage_query = 'SELECT Champion.name as ChampionName, count(Champion.name) as ChampionUsage '\
                                'FROM PlayerInfo JOIN TeamParticipantInfo TPI ON PlayerInfo.summonerID = TPI.summonerID '\
                                'JOIN Team ON Team.gameID = TPI.gameID AND Team.teamID = TPI.teamID '\
                                'JOIN TeamParticipants TP ON TPI.gameID = TP.gameID AND TP.summonerID = TPI.summonerID '\
                                'JOIN Champion ON TP.championID = Champion.championID WHERE PlayerInfo.lolID = :LOLID '\
                                'GROUP BY Champion.name ORDER BY ChampionUsage DESC LIMIT 10'
    user_game_status_query = 'SELECT kills, deaths, assists, largestKillingSpree, largestMultiKill, killingSprees, '\
                             'longestTimeSpentLiving, doubleKills, tripleKills, quadraKills, pentaKills, unrealKills, '\
                             'totalDamageDealt, magicDamageDealt, physicalDamageDealt, trueDamageDealt, largestCriticalStrike, '\
                             'totalDamageDealtToChampions, magicDamageDealtToChampions, physicalDamageDealtToChampions, '\
                             'trueDamageDealtToChampions, totalHeal, totalUnitsHealed, damageSelfMitigated, damageDealtToObjectives, '\
                             'damageDealtToTurrets, visionScore, timeCCingOthers, totalDamageTaken, magicalDamageTaken, '\
                             'physicalDamageTaken, trueDamageTaken, goldEarned, goldSpent, turretKills, inhibitorKills, '\
                             'totalMinionsKilled, neutralMinionsKilled, neutralMinionsKilledTeamJungle, neutralMinionsKilledEnemyJungle, '\
                             'totalTimeCrowdControlDealt, champLevel, visionWardsBoughtInGame, sightWardsBoughtInGame, wardsPlaced, '\
                             'wardsKilled, firstBloodKill, firstBloodAssist, firstTowerKill, firstTowerAssist, firstInhibitorKill, '\
                             'firstInhibitorAssist FROM PlayerInfo INNER JOIN TeamParticipantInfo ON PlayerInfo.summonerID = '\
                             'TeamParticipantInfo.summonerID WHERE lolID = :LOLID AND gameID = :GAMEID'
    user_team_status_query = 'SELECT Team.teamID, Team.firstBlood, Team.firstTower, Team.firstInhibitor, Team.firstBaron, Team.firstDragon, '\
                             'Team.firstRiftHerald, Team.towerKills, Team.inhibitorKills, Team.baronKills, Team.dragonKills, '\
                             'Team.vilemawKills, Team.riftHeraldKills, Team.dominionVictoryScore FROM PlayerInfo INNER JOIN '\
                             'TeamParticipantInfo ON PlayerInfo.summonerID = TeamParticipantInfo.summonerID INNER JOIN '\
                             'Team ON Team.gameID = TeamParticipantInfo.gameID AND Team.teamID = TeamParticipantInfo.teamID '\
                             'WHERE lolID = :LOLID AND Team.gameID = :GAMEID'
    user_enemy_team_status_query = 'SELECT Team.teamID, Team.firstBlood, Team.firstTower, Team.firstInhibitor, Team.firstBaron, Team.firstDragon, '\
                                   'Team.firstRiftHerald, Team.towerKills, Team.inhibitorKills, Team.baronKills, Team.dragonKills, '\
                                   'Team.vilemawKills, Team.riftHeraldKills, Team.dominionVictoryScore FROM PlayerInfo INNER JOIN '\
                                   'TeamParticipantInfo ON PlayerInfo.summonerID = TeamParticipantInfo.summonerID  INNER JOIN Team '\
                                   'ON Team.gameID = TeamParticipantInfo.gameID WHERE Team.teamID != ( select Team.teamID '\
                                   'FROM PlayerInfo INNER JOIN TeamParticipantInfo ON PlayerInfo.summonerID = TeamParticipantInfo.summonerID '\
                                   'INNER JOIN Team ON Team.gameID = TeamParticipantInfo.gameID AND Team.teamID = TeamParticipantInfo.teamID '\
                                   'WHERE lolID = :LOLID AND Team.gameID = :GAMEID) AND lolID = :LOLID AND Team.gameID = :GAMEID'
    user_team_gold_query = 'SELECT SUM(goldEarned) as totalGoldEarned, SUM(goldSpent) as totalGoldSpent FROM TeamParticipantInfo '\
                           'INNER JOIN PlayerInfo ON PlayerInfo.summonerID = TeamParticipantInfo.summonerID WHERE gameID = :GAMEID '\
                           'AND teamID = :TEAMID'


class EntryWithPlaceholder(tk.Entry):
    def __init__(self, master=None, placeholder="PLACEHOLDER", color='grey'):
        super().__init__(master)

        self.placeholder = placeholder
        self.placeholder_color = color
        self.default_fg_color = self['fg']

        self.bind("<FocusIn>", self.foc_in)
        self.bind("<FocusOut>", self.foc_out)

        self.put_placeholder()

    def put_placeholder(self):
        self.insert(0, self.placeholder)
        self['fg'] = self.placeholder_color

    def foc_in(self, *args):
        if self['fg'] == self.placeholder_color:
            self.delete('0', 'end')
            self['fg'] = self.default_fg_color

    def foc_out(self, *args):
        if not self.get():
            self.put_placeholder()

    def check_empty(self):
        if not self.get() or self.get() == self.placeholder:
            return False
        return True


class StackView(tk.Frame):
    # StackView
    def __init__(self, master, *args, **kwargs):
        tk.Frame.__init__(self, master, *args, **kwargs)
        self.stack = []

    def push(self, frame):
        """Push a new frame onto the stack."""
        if self.stack:
            self.stack[-1].pack_forget()
        frame.pack(fill=tk.BOTH, expand=True)
        self.stack.append(frame)

    def pop(self):
        """Pop the top frame off the stack."""
        if self.stack:
            self.stack.pop().pack_forget()
            if self.stack:
                self.stack[-1].pack(fill=tk.BOTH, expand=True)

    def get_stack(self):
        return self.stack
