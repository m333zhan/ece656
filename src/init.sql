create database lolstatus;

use lolstatus;

-- No need to have drop commands since we use docker container and it is easier to just delete the it
-- Game
create table Game (
    gameID decimal(10),
    gameDuration decimal(8) not null check (gameDuration >= 0),
    blueTeamID decimal(3) not null,
    redTeamID decimal(3) not null,
    primary key (gameID),
    check (blueTeamID <> redTeamID)
);

-- PlayerInfo
create table PlayerInfo(
    summonerID varchar(50),
    summonerName varchar(255) not null,
    lolID int unsigned unique,
    primary key (summonerID)
);

-- Champion
create table Champion (
    championID decimal(5),
    name varchar(50) not null,
    title varchar(50) not null,
    blurb text not null,
    partype varchar(50) not null,
    difficulty decimal(2) not null check (difficulty between 1 and 10),
    tag varchar(50) not null,
    primary key (championID)
);

-- Team
create table Team (
    gameID decimal(10),
    teamID decimal(3),
    win bit not null,
    firstBlood bit not null,
    firstTower bit not null,
    firstInhibitor bit not null,
    firstBaron bit not null,
    firstDragon bit not null,
    firstRiftHerald bit not null,
    towerKills decimal(5) not null check (towerKills >= 0),
    inhibitorKills decimal(5) not null check (inhibitorKills >= 0),
    baronKills decimal(5) not null check (baronKills >= 0),
    dragonKills decimal(5) not null check (dragonKills >= 0),
    vilemawKills decimal(5) not null check (vilemawKills >= 0),
    riftHeraldKills decimal(5) not null check (riftHeraldKills >= 0),
    dominionVictoryScore decimal(5) not null check (dominionVictoryScore >= 0),
    primary key (gameID, teamID),
    foreign key(gameID) references Game(gameID) on update CASCADE on delete RESTRICT
);

-- TeamBan
-- championID here will not be a forgine key since it could be -1
create table TeamBan (
    gameID decimal(10),
    teamID decimal(3),
    pickTurn decimal(2),
    championID decimal(5) not null,
    primary key (gameID, teamID, pickTurn),
    foreign key(gameID) references Game(gameID) on update CASCADE on delete RESTRICT
);

-- TeamParticipants
create table TeamParticipants (
    gameID decimal(10),
    teamID decimal(3),
    participantID decimal(2),
    summonerID varchar(50),
    role varchar(20) not null,
    lane varchar(20),
    championID decimal(5) not null,
    primary key (gameID, teamID, participantID, summonerID),
    foreign key(gameID) references Game(gameID) on update CASCADE on delete RESTRICT,
    foreign key(summonerID) references PlayerInfo(summonerID) on update CASCADE on delete RESTRICT,
    foreign key(championID) references Champion(championID) on update CASCADE on delete RESTRICT
);

-- TeamParticipantInfo
create table TeamParticipantInfo (
    gameID decimal(10),
    teamID decimal(3),
    participantID decimal(2),
    summonerID varchar(50),
    item0 decimal(4),
    item1 decimal(4),
    item2 decimal(4),
    item3 decimal(4),
    item4 decimal(4),
    item5 decimal(4),
    item6 decimal(4),
    kills decimal(4) not null,
    deaths decimal(4) not null,
    assists decimal(4) not null,
    largestKillingSpree decimal(4) not null,
    largestMultiKill decimal(4) not null,
    killingSprees decimal(4) not null,
    longestTimeSpentLiving decimal(5) not null,
    doubleKills decimal(5) not null,
    tripleKills decimal(5) not null,
    quadraKills decimal(5) not null,
    pentaKills decimal(5) not null,
    unrealKills decimal(5) not null,
    totalDamageDealt decimal(10) not null,
    magicDamageDealt decimal(10) not null,
    physicalDamageDealt decimal(10) not null,
    trueDamageDealt decimal(10) not null,
    largestCriticalStrike decimal(5) not null,
    totalDamageDealtToChampions decimal(10) not null,
    magicDamageDealtToChampions decimal(10) not null,
    physicalDamageDealtToChampions decimal(10) not null,
    trueDamageDealtToChampions decimal(10) not null,
    totalHeal decimal(10) not null,
    totalUnitsHealed decimal(5) not null,
    damageSelfMitigated decimal(10) not null,
    damageDealtToObjectives decimal(10) not null,
    damageDealtToTurrets decimal(10) not null,
    visionScore decimal(10) not null,
    timeCCingOthers decimal(10) not null,
    totalDamageTaken decimal(10) not null,
    magicalDamageTaken decimal(10) not null,
    physicalDamageTaken decimal(10) not null,
    trueDamageTaken decimal(10) not null,
    goldEarned decimal(10) not null,
    goldSpent decimal(10) not null,
    turretKills decimal(10) not null,
    inhibitorKills decimal(10) not null,
    totalMinionsKilled decimal(10) not null,
    neutralMinionsKilled decimal(10) not null,
    neutralMinionsKilledTeamJungle decimal(10) not null,
    neutralMinionsKilledEnemyJungle decimal(10) not null,
    totalTimeCrowdControlDealt decimal(10) not null,
    champLevel decimal(2) not null,
    visionWardsBoughtInGame decimal(10) not null,
    sightWardsBoughtInGame decimal(10) not null,
    wardsPlaced decimal(10) not null,
    wardsKilled decimal(10) not null,
    firstBloodKill bit not null,
    firstBloodAssist bit not null,
    firstTowerKill bit not null,
    firstTowerAssist bit not null,
    firstInhibitorKill bit not null,
    firstInhibitorAssist bit not null,
    primary key (gameID, teamID, participantID, summonerID),
    foreign key(gameID) references Game(gameID) on update CASCADE on delete RESTRICT,
    foreign key(summonerID) references PlayerInfo(summonerID) on update CASCADE on delete RESTRICT
);

-- ChampionInfo
create table ChampionInfo (
    championID decimal(5),
    baseAttack decimal(5) not null,
    baseDefense decimal(5) not null,
    baseMagic decimal(5) not null,
    hp decimal(5) not null,
    hpPerLevel decimal(5) not null,
    mp decimal(5) not null,
    mpPerLevel decimal(5) not null,
    moveSpeed decimal(5) not null,
    armor decimal(5) not null,
    armorPerLevel decimal(5) not null,
    spellBlock decimal(5) not null,
    spellBlockPerLevel decimal(5) not null,
    attackRange decimal(5) not null,
    hpRegen decimal(5) not null,
    hpRegenPerLevel decimal(5) not null,
    mpRegen decimal(5) not null,
    mpRegenPerLevel decimal(5) not null,
    attackDamage decimal(5) not null,
    attackDamagerPerLevel decimal(5) not null,
    attackSpeedPerLevel decimal(5) not null,
    attackSpeed decimal(5) not null,
    primary key (championID),
    foreign key(championID) references Champion(championID) on update CASCADE on delete RESTRICT
);

-- Item
create table Item (
    itemID decimal(5),
    name varchar(50) not null,
    explaination varchar(255),
    buyPrice decimal(4) not null,
    sellPrice decimal(4) not null,
    tag varchar(50),
    primary key (itemID)
);

CREATE INDEX idx_playerinfo_summonername ON PlayerInfo(summonerName);
CREATE INDEX idx_champion_name ON Champion(name);
CREATE INDEX idx_teamparticipants_summonerid ON TeamParticipants(summonerID);
CREATE INDEX idx_teamparticipantinfo_summonerid ON TeamParticipantInfo(summonerID);
CREATE INDEX idx_item_name ON Item(name);

-- Add lolID
DELIMITER //

CREATE TRIGGER set_lolID
BEFORE INSERT ON PlayerInfo
FOR EACH ROW
BEGIN
  DECLARE next_lolID INT UNSIGNED DEFAULT 100000;
  SELECT MAX(lolID) + 1 INTO next_lolID FROM PlayerInfo;
  IF next_lolID IS NULL THEN
    SET new.lolID = 100000;
  ELSE
    SET new.lolID = next_lolID;
  END IF;
END //

DELIMITER ;