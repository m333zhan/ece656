import unittest
import tkinter as tk
import pandas as pd
import warnings
from app import App
from sqlalchemy import text
from unittest.mock import patch, MagicMock
from sqlalchemy import Connection
from appConst import appConst as AppConst


class TestApp(unittest.TestCase):

    @patch('builtins.print')
    def setUp(self, mock_print):
        # Ignore warnings since we are using mock sqlalchemy Connection
        warnings.filterwarnings("ignore")

        # Mock sqlalchemy Connection
        def new_connect_db():
            new_mock_mysql_connection = MagicMock(
                spec=Connection, return_value='patched')
            return new_mock_mysql_connection

        with patch('app.create_conn', new_connect_db):
            self.root = tk.Tk()
            self.app = App(self.root)

    def tearDown(self):
        self.root.destroy()

    def test_stack_view(self):
        # Test stack view order when invoking buttons

        self.app.update_name_button.invoke()
        self.assertEqual(self.app.stack_view.get_stack()
                         [-1], self.app.update_name_frame)
        self.app.back_button_update_name.invoke()
        self.assertEqual(self.app.stack_view.get_stack()
                         [-1], self.app.start_frame)

        self.app.champion_page_button.invoke()
        self.assertEqual(self.app.stack_view.get_stack()
                         [-1], self.app.champion_frame)
        self.app.back_button_champion.invoke()
        self.assertEqual(self.app.stack_view.get_stack()
                         [-1], self.app.start_frame)

        self.app.item_page_button.invoke()
        self.assertEqual(self.app.stack_view.get_stack()
                         [-1], self.app.item_frame)
        self.app.back_button_item.invoke()
        self.assertEqual(self.app.stack_view.get_stack()
                         [-1], self.app.start_frame)

        self.app.match_page_button.invoke()
        self.assertEqual(self.app.stack_view.get_stack()
                         [-1], self.app.match_frame)
        self.app.back_button_match_root.invoke()
        self.assertEqual(self.app.stack_view.get_stack()
                         [-1], self.app.start_frame)

        self.app.match_pred_page_button.invoke()
        self.assertEqual(self.app.stack_view.get_stack()
                         [-1], self.app.match_pred_frame)
        self.app.back_button_pred.invoke()
        self.assertEqual(self.app.stack_view.get_stack()
                         [-1], self.app.start_frame)

    @patch('pandas.read_sql')
    def test_show_detailes_champion_valid_id(self, mock_read_sql):
        # Set up mock data for champion details and story
        champion_details_data = {
            'id': ['Aatrox'],
            'name': ['The Darkin Blade'],
            'title': ['Darkin Blade'],
            'tags': ['Fighter, Tank'],
            'attackdamage': ['60'],
            'attackspeed': ['0.651'],
            'armor': ['38'],
            'magicresist': ['32.1'],
            'hp': ['580'],
            'mp': ['0'],
            'movespeed': ['345'],
            'attackrange': ['175'],
            'champion_id': ['Aatrox']
        }
        champion_story_data = {
            'id': ['Aatrox'],
            'blurb': ['Once honored defenders of Shurima against the Void, Aatrox and his brethren would eventually\
                       become an even greater threat to Runeterra, \
                       and were defeated only by cunning mortal sorcery.\
                       But after centuries of imprisonment, Aatrox was the first to find...']
        }
        champion_id = 'Aatrox'

        # Mock sqlalchemy Connection
        def new_connect_db():
            new_mock_mysql_connection = MagicMock(
                spec=Connection, return_value='patched')
            return new_mock_mysql_connection
        # Mock pd.read_sql
        mock_read_sql.side_effect = [pd.DataFrame({'COUNT(championID)': 1}, index=[0]),
                                     pd.DataFrame(champion_details_data), pd.DataFrame(champion_story_data)]

        with patch('app.create_conn', new_connect_db):
            self.app.details_champion_text_input.insert(0, champion_id)
            self.app.show_detailes_champion()

        # Check that the Treeview and label widgets were updated correctly
        expected_columns = list(champion_details_data.keys())
        self.assertEqual(
            list(self.app.champion_details_table['columns']), expected_columns)
        self.assertEqual(self.app.champion_details_table.item(
            self.app.champion_details_table.get_children()[0], 'values')[0], list(champion_details_data.values())[0][0])
        assert (len(self.app.champion_story_label.get('1.0', tk.END)) > 0)

    @patch('pandas.read_sql')
    def test_show_detailes_champion_invalid_id(self, mock_read_sql):
        champion_id = 'InvalidChampionID'

        # Set up mock database connection and queries
        mock_conn = MagicMock()
        mock_cursor = MagicMock()
        mock_conn.cursor.return_value = mock_cursor
        mock_cursor.execute.return_value = ()

        # Mock pd.read_sql
        mock_read_sql.side_effect = [pd.DataFrame(
            {'COUNT(championID)': 0}, index=[0])]

        with patch('app.create_conn', return_value=mock_conn):
            self.app.details_champion_text_input.insert(0, champion_id)
            self.app.show_detailes_champion()

        # Check that the text input was cleared and error message was displayed
        self.assertEqual(
            self.app.details_champion_text_input.get(), 'Invalid Champion ID!!!!')
        self.assertEqual(self.app.champion_details_table.get_children(), ())
        self.assertEqual(
            self.app.champion_story_label.get('1.0', tk.END), '\n')

    @patch('pandas.read_sql')
    def test_show_user_match_status_frame(self, mock_read_sql):
        # Set up mock database connection and queries
        mock_conn = MagicMock()
        mock_cursor = MagicMock()
        mock_conn.cursor.return_value = mock_cursor
        mock_cursor.execute.return_value = ()
        with patch('app.create_conn'):
            # Mock pd.read_sql
            mock_read_sql.side_effect = [pd.DataFrame({'COUNT(lolID)': 1}, index=[0]),
                                         pd.DataFrame(
                                             {'summonerName': 'Name'}, index=[0]),
                                         pd.DataFrame(
                                             {'win_rate': 0}, index=[0]),
                                         pd.DataFrame(
                                             {'gameID': 100002, 'win': b'0x01', 'lane': 'TOP', 'role': 'SOLO'}, index=[0, 1, 2, 3]),
                                         pd.DataFrame(
                                             {'totalKills': 0, 'totalDeaths': 0, 'totalAssists': 0, 'totalDoubleKills': 0,
                                              'totalTripleKills': 0, 'totalQuadraKills': 0, 'totalPentaKills': 0}, index=[0, 1, 2, 3, 4, 5, 6]),
                                         pd.DataFrame({'ChampionUsage': 50, 'ChampionName': 'TestName'}, index=[0, 1])]

            self.app.show_user_match_status_frame()

        self.assertEqual(self.app.winning_rate_label.cget(
            "text"), 'Winning Rate: 0%')
        self.assertEqual(self.app.numbers_of_games_played_label.cget(
            "text"), 'Number of games played: 16')
        self.assertEqual(self.app.death_kill_assist_table.item(
            self.app.death_kill_assist_table.get_children()[0], 'values')[0], '0')
        self.assertEqual(self.app.user_match_status_history_table.item(
            self.app.user_match_status_history_table.get_children()[0], 'values')[0], '100002')
