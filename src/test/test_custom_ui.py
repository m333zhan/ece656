import unittest
import tkinter as tk
from app import App
from tkinter import ttk
from appConst import appConst as AppConst


class TestEntryWithPlaceholder(unittest.TestCase):
    # Test EntryWithPlaceholder
    def setUp(self):
        self.root = tk.Tk()

    def tearDown(self):
        self.root.destroy()

    def test_placeholder_text(self):
        entry = AppConst.EntryWithPlaceholder(
            self.root, placeholder='Test Placeholder')
        self.assertEqual(entry.get(), 'Test Placeholder')

    def test_placeholder_color(self):
        entry = AppConst.EntryWithPlaceholder(
            self.root, placeholder='Test Placeholder', color='red')
        self.assertEqual(entry['fg'], 'red')

    def test_default_color(self):
        entry = AppConst.EntryWithPlaceholder(self.root)
        self.assertEqual(entry['fg'], 'grey')

    def test_placeholder_deleted_on_focus(self):
        entry = AppConst.EntryWithPlaceholder(
            self.root, placeholder='Test Placeholder')
        entry.foc_in()
        self.assertEqual(entry.get(), '')

    def test_placeholder_reinserted_on_focus_out(self):
        entry = AppConst.EntryWithPlaceholder(
            self.root, placeholder='Test Placeholder')
        entry.foc_in()
        entry.foc_out()
        self.assertEqual(entry.get(), 'Test Placeholder')

    def test_check_empty_with_input(self):
        entry = AppConst.EntryWithPlaceholder(self.root)
        entry.insert(0, 'Test Input')
        self.assertTrue(entry.check_empty())

    def test_check_empty_with_placeholder(self):
        entry = AppConst.EntryWithPlaceholder(
            self.root, placeholder='Test Placeholder')
        self.assertFalse(entry.check_empty())


class TestStackView(unittest.TestCase):
    # Test StackView
    def setUp(self):
        self.root = tk.Tk()
        self.stack_view = AppConst.StackView(self.root)

    def tearDown(self):
        self.root.destroy()

    def test_push_adds_frame(self):
        frame = ttk.Frame(self.stack_view)
        self.stack_view.push(frame)
        self.assertIn(frame, self.stack_view.stack)

    def test_push_pack_options(self):
        frame = ttk.Frame(self.stack_view)
        self.stack_view.push(frame)
        self.assertTrue(frame.winfo_ismapped() == 0)
        self.assertEqual(frame.pack_info()['fill'], 'both')
        self.assertEqual(frame.pack_info()['expand'], True)

    def test_push_replaces_current_frame(self):
        frame1 = ttk.Frame(self.stack_view)
        frame2 = ttk.Frame(self.stack_view)
        self.stack_view.push(frame1)
        self.stack_view.push(frame2)
        self.stack_view.pop()
        self.assertIn(frame1, self.stack_view.stack)
        self.assertNotIn(frame2, self.stack_view.stack)

    def test_pop_removes_top_frame(self):
        frame = ttk.Frame(self.stack_view)
        self.stack_view.push(frame)
        self.stack_view.pop()
        self.assertNotIn(frame, self.stack_view.stack)

    def test_pop_packs_previous_frame(self):
        frame1 = ttk.Frame(self.stack_view)
        frame2 = ttk.Frame(self.stack_view)
        self.stack_view.push(frame1)
        self.stack_view.push(frame2)
        self.stack_view.pop()
        self.assertTrue(frame1.winfo_ismapped() == 0)
        self.assertEqual(frame1.pack_info()['fill'], 'both')
        self.assertEqual(frame1.pack_info()['expand'], True)
