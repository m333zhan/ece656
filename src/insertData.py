import mysql.connector
import csv
import json
from appConst import appConst as AppConst

# Define the database configuration
db_config = {
    'host': AppConst.DBConst.db_connect_host,
    'user': AppConst.DBConst.db_connect_user_name,
    'port': AppConst.DBConst.db_connect_port,
    'password': AppConst.DBConst.db_connect_password,
    'database': AppConst.DBConst.db_connect_db_name
}

# itemData
try:
    # Connect to the database
    print("Loading itemData...")
    with mysql.connector.connect(**db_config) as connection:
        with connection.cursor() as cursor:
            with open('../data/itemData.csv', 'r') as csvfile:
                csvreader = csv.reader(csvfile)
                # Skip header row
                next(csvreader)
                for row in csvreader:
                    item_id = int(row[0])
                    name = row[1]
                    explaination = row[3]
                    buy_price = int(row[4])
                    sell_price = int(row[5])
                    tag = row[6]
                    sql_item = "insert into Item (itemID, name, explaination, buyPrice, sellPrice, tag) "\
                               "VALUES (%s, %s, %s, %s, %s, %s)"
                    values = (item_id, name, explaination,
                              buy_price, sell_price, tag)
                    cursor.execute(sql_item, values)
                    # Commit the transaction
                    connection.commit()
except Exception as e:
    print("An error occurred (itemData):", e)

# championData
try:
    # Connect to the database
    print("Loading championData...")
    with mysql.connector.connect(**db_config) as connection:
        with connection.cursor() as cursor:
            with open('../data/championData.csv', 'r') as csvfile:
                csvreader = csv.reader(csvfile)
                # Skip header row
                next(csvreader)
                for row in csvreader:
                    champion_id = int(row[1])
                    name = row[2]
                    title = row[3]
                    blurb = row[4]
                    tags = row[5]
                    partype = row[6]
                    base_attack = float(row[7])
                    base_defense = float(row[8])
                    base_magic = float(row[9])
                    difficulty = float(row[10])
                    hp = float(row[11])
                    hp_per_level = float(row[12])
                    mp = float(row[13])
                    mp_per_level = float(row[14])
                    move_speed = float(row[15])
                    armor = float(row[16])
                    armor_per_level = float(row[17])
                    spell_block = float(row[18])
                    spell_block_per_level = float(row[19])
                    attack_range = float(row[20])
                    hp_regen = float(row[21])
                    hp_regen_per_level = float(row[22])
                    mp_regen = float(row[23])
                    mp_regen_per_level = float(row[24])
                    attack_damage = float(row[25])
                    attack_damage_per_level = float(row[26])
                    attack_speed_per_level = float(row[27])
                    attack_speed = float(row[28])

                    # Champion insert
                    sql_champion = "insert into Champion (championID, name, title, blurb, partype, difficulty, tag) "\
                                   "VALUES (%s, %s, %s, %s, %s, %s, %s)"
                    values = (champion_id, name, title, blurb,
                              partype, difficulty, tags)
                    cursor.execute(sql_champion, values)

                    # ChampionInfo
                    sql_champion_info = "insert into ChampionInfo (championID, baseAttack, baseDefense, baseMagic, hp, "\
                                        "hpPerLevel, mp, mpPerLevel, moveSpeed, armor, armorPerLevel, spellBlock, "\
                                        "spellBlockPerLevel, attackRange, hpRegen, hpRegenPerLevel, mpRegen, "\
                                        "mpRegenPerLevel, attackDamage, attackDamagerPerLevel, attackSpeedPerLevel, attackSpeed) "\
                                        "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                    values = (champion_id, base_attack, base_defense, base_magic, hp, hp_per_level,
                              mp, mp_per_level, move_speed, armor, armor_per_level, spell_block,
                              spell_block_per_level, attack_range, hp_regen, hp_regen_per_level,
                              mp_regen, mp_regen_per_level, attack_damage, attack_damage_per_level,
                              attack_speed_per_level, attack_speed)
                    cursor.execute(sql_champion_info, values)
                    # Commit the transaction
                    connection.commit()
except Exception as e:
    print("An error occurred (championData):", e)

# matchData
try:
    def game_insert(game_id, game_duration, cursor):
        # Game insert
        sql_game = "insert into Game (gameID, gameDuration, blueTeamID, redTeamID) VALUES (%s, %s, %s, %s)"
        values = (game_id, game_duration, 100, 200)
        cursor.execute(sql_game, values)

    def team_team_ban_insert(teams, cursor):
        # Team, TeamBan insert
        for team in teams:
            # Team
            sql_team = "insert into Team "\
                       "(gameID, teamID, win, firstBlood, firstTower, firstInhibitor, "\
                       "firstBaron, firstDragon, firstRiftHerald, towerKills, inhibitorKills, "\
                       "baronKills, dragonKills, vilemawKills, riftHeraldKills, dominionVictoryScore) "\
                       "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            values = (
                game_id, team['teamId'], True if team['win'] == 'Win' else False, team['firstBlood'],
                team['firstTower'], team['firstInhibitor'], team['firstBaron'], team['firstDragon'],
                team['firstRiftHerald'], team['towerKills'], team['inhibitorKills'], team['baronKills'],
                team['dragonKills'], team['vilemawKills'], team['riftHeraldKills'], team['dominionVictoryScore']
            )
            cursor.execute(sql_team, values)

            for ban in team['bans']:
                # TeamBan
                sql_team_ban = "insert into TeamBan "\
                               "(gameID, teamID, pickTurn, championID) "\
                               "VALUES (%s, %s, %s, %s)"
                values = (
                    game_id, team['teamId'], ban['pickTurn'], ban['championId']
                )
                cursor.execute(sql_team_ban, values)

    # The reason not doing this on kaggle is to avoid going over large dataset twice
    def team_participant_info_missing_value_handle(participant):
        if 'firstInhibitorKill' not in participant['stats']:
            participant['stats']['firstInhibitorKill'] = AppConst.MissingValueConst.bool_value
        if 'firstInhibitorAssist' not in participant['stats']:
            participant['stats']['firstInhibitorAssist'] = AppConst.MissingValueConst.bool_value
        if 'neutralMinionsKilledTeamJungle' not in participant['stats']:
            participant['stats']['neutralMinionsKilledTeamJungle'] = AppConst.MissingValueConst.int_value
        if 'neutralMinionsKilledEnemyJungle' not in participant['stats']:
            participant['stats']['neutralMinionsKilledEnemyJungle'] = AppConst.MissingValueConst.int_value
        if 'wardsPlaced' not in participant['stats']:
            participant['stats']['wardsPlaced'] = AppConst.MissingValueConst.int_value
        if 'wardsKilled' not in participant['stats']:
            participant['stats']['wardsKilled'] = AppConst.MissingValueConst.int_value
        if 'firstTowerKill' not in participant['stats']:
            participant['stats']['firstTowerKill'] = AppConst.MissingValueConst.bool_value
        if 'firstTowerAssist' not in participant['stats']:
            participant['stats']['firstTowerAssist'] = AppConst.MissingValueConst.bool_value
        if 'firstBloodKill' not in participant['stats']:
            participant['stats']['firstBloodKill'] = AppConst.MissingValueConst.bool_value
        if 'firstBloodAssist' not in participant['stats']:
            participant['stats']['firstBloodAssist'] = AppConst.MissingValueConst.bool_value

    def team_participants_player_info_insert(game_id, participants_identities, participants, cursor):
        # PlayerInfo
        participant_participant_id_summoner_id_cache = {}
        for participant_identity in participants_identities:
            participant_participant_id_summoner_id_cache[participant_identity[
                'participantId']] = participant_identity['player']['summonerId']
            sql_player_info = "insert ignore into PlayerInfo (summonerID, summonerName) "\
                              "VALUES (%s, %s)"
            values = (
                participant_identity['player']['summonerId'],
                participant_identity['player']['summonerName']
            )
            cursor.execute(sql_player_info, values)

        # TeamParticipants, TeamParticipantInfo
        # Note:
        # in the original data, there are some missing fields
        # we will give it a default value from appConst
        for participant in participants:
            # TeamParticipants
            sql_team_participants = "insert into TeamParticipants "\
                                    "(gameID, teamID, participantID, summonerID, championID, role, lane) "\
                                    "VALUES (%s, %s, %s, %s, %s, %s, %s)"
            values = (
                game_id, participant['teamId'], participant['participantId'],
                participant_participant_id_summoner_id_cache[participant['participantId']],
                participant['championId'], participant['role'], participant['lane']
            )
            cursor.execute(sql_team_participants, values)

            # TeamParticipantInfo
            # Missing value handle
            team_participant_info_missing_value_handle(participant)

            sql_team_participants_info = "insert into TeamParticipantInfo "\
                                         "(gameID, teamID, participantID, summonerID, item0, item1, item2, "\
                                         "item3, item4, item5, item6, kills, deaths, assists, largestKillingSpree, "\
                                         "largestMultiKill, killingSprees, longestTimeSpentLiving, doubleKills, "\
                                         "tripleKills, quadraKills, pentaKills, unrealKills, totalDamageDealt, "\
                                         "magicDamageDealt, physicalDamageDealt, trueDamageDealt, largestCriticalStrike, "\
                                         "totalDamageDealtToChampions, magicDamageDealtToChampions, physicalDamageDealtToChampions, "\
                                         "trueDamageDealtToChampions, totalHeal, totalUnitsHealed, damageSelfMitigated, "\
                                         "damageDealtToObjectives, damageDealtToTurrets, visionScore, timeCCingOthers, "\
                                         "totalDamageTaken, magicalDamageTaken, physicalDamageTaken, trueDamageTaken, "\
                                         "goldEarned, goldSpent, turretKills, inhibitorKills, totalMinionsKilled, "\
                                         "neutralMinionsKilled, neutralMinionsKilledTeamJungle, neutralMinionsKilledEnemyJungle, "\
                                         "totalTimeCrowdControlDealt, champLevel, visionWardsBoughtInGame, sightWardsBoughtInGame, "\
                                         "wardsPlaced, wardsKilled, firstBloodKill, firstBloodAssist, firstTowerKill, "\
                                         "firstTowerAssist, firstInhibitorKill, firstInhibitorAssist) "\
                                         "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "\
                                         "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "\
                                         "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "\
                                         "%s, %s)"
            values = (
                game_id, participant['teamId'], participant['participantId'],
                participant_participant_id_summoner_id_cache[participant['participantId']],
                participant['stats']['item0'], participant['stats']['item1'], participant['stats']['item2'],
                participant['stats']['item3'], participant['stats']['item4'], participant['stats']['item5'],
                participant['stats']['item6'], participant['stats']['kills'], participant['stats']['deaths'],
                participant['stats']['assists'], participant['stats']['largestKillingSpree'],
                participant['stats']['largestMultiKill'], participant['stats']['killingSprees'],
                participant['stats']['longestTimeSpentLiving'], participant['stats']['doubleKills'],
                participant['stats']['tripleKills'], participant['stats']['quadraKills'],
                participant['stats']['pentaKills'], participant['stats']['unrealKills'],
                participant['stats']['totalDamageDealt'], participant['stats']['magicDamageDealt'],
                participant['stats']['physicalDamageDealt'], participant['stats']['trueDamageDealt'],
                participant['stats']['largestCriticalStrike'], participant['stats']['totalDamageDealtToChampions'],
                participant['stats']['magicDamageDealtToChampions'], participant['stats']['physicalDamageDealtToChampions'],
                participant['stats']['trueDamageDealtToChampions'], participant['stats']['totalHeal'],
                participant['stats']['totalUnitsHealed'], participant['stats']['damageSelfMitigated'],
                participant['stats']['damageDealtToObjectives'], participant['stats']['damageDealtToTurrets'],
                participant['stats']['visionScore'], participant['stats']['timeCCingOthers'],
                participant['stats']['totalDamageTaken'], participant['stats']['magicalDamageTaken'],
                participant['stats']['physicalDamageTaken'], participant['stats']['trueDamageTaken'],
                participant['stats']['goldEarned'], participant['stats']['goldSpent'], participant['stats']['turretKills'],
                participant['stats']['inhibitorKills'], participant['stats']['totalMinionsKilled'],
                participant['stats']['neutralMinionsKilled'], participant['stats']['neutralMinionsKilledTeamJungle'],
                participant['stats']['neutralMinionsKilledEnemyJungle'], participant['stats']['totalTimeCrowdControlDealt'],
                participant['stats']['champLevel'], participant['stats']['visionWardsBoughtInGame'],
                participant['stats']['sightWardsBoughtInGame'], participant['stats']['wardsPlaced'],
                participant['stats']['wardsKilled'], participant['stats']['firstBloodKill'],
                participant['stats']['firstBloodAssist'], participant['stats']['firstTowerKill'],
                participant['stats']['firstTowerAssist'], participant['stats']['firstInhibitorKill'],
                participant['stats']['firstInhibitorAssist']
            )
            cursor.execute(sql_team_participants_info, values)

    # Connect to the database
    with mysql.connector.connect(**db_config) as connection:
        with connection.cursor() as cursor:
            for part in range(1, AppConst.MiscConst.max_match_data_part + 1):

                print('Loading matchDataPart' + str(part) + '...')
                file_path = '../data/matchDataPart' + str(part) + '.csv'

                with open(file_path, 'r') as csvfile:
                    csvreader = csv.reader(csvfile)
                    # Skip header row
                    next(csvreader)

                    counter = 1
                    for row in csvreader:
                        print(f"\rProcessing row: {counter}", end="")
                        counter = counter + 1

                        # gameDuration gameID
                        game_duration = float(row[0])
                        game_id = row[1]
                        # Convert the JSON string to python format
                        # participantIdentities
                        json_participants_identities = row[2].replace(
                            "'", "\"")
                        participants_identities = json.loads(
                            json_participants_identities)

                        # participants, also need to replace True to true
                        # False to false for json to parse
                        json_participants = row[3].replace("'", "\"").replace(
                            'True', 'true').replace('False', 'false')
                        participants = json.loads(json_participants)
                        # teams
                        json_teams = row[4].replace("'", "\"").replace(
                            'True', 'true').replace('False', 'false')
                        teams = json.loads(json_teams)

                        # Game insert
                        game_insert(game_id, game_duration, cursor)

                        # Team, TeamBan insert
                        team_team_ban_insert(teams, cursor)

                        # TeamParticipants, PlayerInfo, TeamParticipantInfo insert
                        team_participants_player_info_insert(
                            game_id, participants_identities, participants, cursor)

                        # Commit the transaction
                        connection.commit()
                    print("\n")
except Exception as e:
    print("\n")
    print("An error occurred (matchData):", e)
