# ECE656 - League of Legends - Who will be the winner?

## Online Doc

- Proposal: [ECE656 - Project Proposal](https://docs.google.com/document/d/1ylg0AxpV7z7vX9syvB8CfEDUxWU8dkw8M7SQfiuDGiI/edit?usp=sharing)
- Project: [ECE656 - League of Legends - Who will be the winner?](https://www.overleaf.com/read/fzsfjmbtccsw)
- [ER](https://lucid.app/lucidchart/db41fe70-7b63-41f0-879a-e5dce04ff134/edit?viewport_loc=-450%2C7466%2C3372%2C1598%2C0_0&invitationId=inv_6788f870-ba49-4a9d-a069-5f6e8d2dc2cc)
- [UI Flow](https://lucid.app/lucidchart/b381ba53-035d-4fe8-ab14-738b9c0b4106/edit?viewport_loc=21%2C-1386%2C2248%2C1065%2C0_0&invitationId=inv_a19dd3a4-4013-4b50-b43b-4fd83d73903c)

## Data

- Dataset One: [League of Legends(LOL) - Ranked Games 2020](https://www.kaggle.com/datasets/gyejr95/league-of-legendslol-ranked-games-2020-ver1)
- Dataset Two: [League of Legends(LOL) CHAMPION and ITEM - 2020](https://www.kaggle.com/datasets/gyejr95/league-of-legendslol-champion-and-item-2020)

> We will not push the datasets to the repo since they are large, but they should exist under the ```data``` directory in our design.

## Data Preprocessing

- [Kaggle Online Data Preprocessing](https://www.kaggle.com/code/mingruizhangrr/ece656-lol-data-preprocessing/notebook)

> Note: the project on Kaggle is now private, we keep a copy of the python notebook under ```src/kaggleNotebook``` (Better to render locally since Gitlab does not render markdown properly for this notebook)

## Mysql Setup Command

```sh
    # Build - root folder
    docker compose -f ./docker/docker-compose.yml up --build
    # Clean - root folder
    docker-compose -f ./docker/docker-compose.yml down --rmi all --volumes --remove-orphans
```

## Python scripts

```sh
    cd src
    # Insert data into the database, need docker mysql container running
    python3 insertData.py
    # Train the decision tree model, need docker mysql container running
    python3 train.py
    # Run the client app, need docker mysql container running
    python3 app.py
```

## Data-Mining

The data in the ```Team``` table is utilized to train a decision tree model for predicting the winning team based on their status.

- Import the model directly from ```src/model```

```py
    model = joblib.load('model.pkl')
```

- Train yourself
    - Use ```train.py```, it will generate a ```Mean Accuracy vs. Tree Depth``` graph when training to help us find the best tree depth.
    - It will generate ```model.pkl``` into the ```src/model``` folder
    - It will generate a visualizable ```tree.dot.png``` in the ```doc``` folder with the raw data ```tree.dot```.

## Unit tests

```sh
    cd src
    python3 -m unittest
```

## Python code local runtime

- Windows WSL or MacOS, [python](https://www.python.org/downloads/windows/)
- ```pip install mysql-connector-python```
- ```pip install graphviz```
- ```pip install sqlalchemy```
- ```pip install tk```
- ```pip install matplotlib```
- ```pip install joblib```
- ```pip install pymysql```
- ```pip install -U scikit-learn```
